﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Content.aspx.cs" EnableEventValidation="false"
    ValidateRequest="false" Inherits="PowerEasy.Module.General.WebSite.User.Content.Content" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
    <pe:ExtendedSiteMapPath ID="YourPosition" SiteMapProvider="UserMapProvider" runat="server" />
    <form id="MainForm" runat="server">
        <asp:ScriptManager ID="SmContent" runat="server" ScriptMode="Release" EnablePartialRendering="true">
        </asp:ScriptManager>
        <div class="model_content_wrap">
            <div class="model_content_top">
                <h3>
                    <pe:AlternateLiteral ID="LblTitle" Text="<%$ Res: User_Contents_Content_LblTitle,添加内容信息 %>"
                        AlternateText="<%$ Res: User_Contents_Content_LblTitle_AlternateText,修改内容信息 %>"
                        runat="Server" />
                </h3>
            </div>
            <div class="model_content_center">
                <ul class="add_model_content">
                    <pe:ExtendedRepeater ID="ContentForm" OnItemDataBound="ContentForm_OnItemDataBound"
                        runat="server">
                        <ItemTemplate>
                            <li>
                                <label>
                                    <%# Eval("FieldAlias")%>：<%# (Eval("Tips") == null || string.IsNullOrEmpty(Eval("Tips").ToString())) ? "" : "<br />" + Eval("Tips").ToString()%></label>
                                <pe:FieldControl ID="Field" runat="server" EnableNull='<%# (bool)Eval("EnableNull") %>'
                                    FieldAlias='<%# Eval("FieldAlias")%>' Tips='<%# Eval("Tips") %>' FieldName='<%#Eval("FieldName")%>'
                                    ControlType='<%# Eval("FieldType") %>' FieldLevel='<%# Eval("FieldLevel") %>' ModelId='<%# Eval("ModelId") %>'
                                    IsAdminManage="false" Description='<%# Eval("Description")%>' Settings='<%# ((PowerEasy.Module.General.Model.CommonModel.FieldInfo)Container.DataItem).Settings %>'
                                    Value='<%# Eval("DefaultValue") %>'>
                                </pe:FieldControl>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            <li id="EditorDutyTab" runat="server" visible="false">
                                <label>
                                    信息录入责任：</label>
                                <span class="add_model_content_radiobuttonlist"><asp:CheckBox ID="ChkEditorDuty" runat="server" ></asp:CheckBox><span style="color: Red;">* </span>
                                <asp:CustomValidator ID="CustomValidator1" runat="server" ForeColor="Red" ErrorMessage="请勾选！" ClientValidationFunction="checkIsNotnull"></asp:CustomValidator>
                                <script language="javascript" type="text/javascript">
                                    //验证checkbox不空
                                    function checkIsNotnull(source, args) {
                                        if ($("*[id$=ChkEditorDuty]").prop("checked")) {
                                            args.IsValid = true;
                                            return;
                                        }
                                        args.IsValid = false;
                                    }
                                </script></span>
                            </li>
                        </FooterTemplate>
                    </pe:ExtendedRepeater>
                </ul>
                <div id="wordpaster-container"></div>
                <!--WordPaster begin-->
                <link type="text/css" rel="Stylesheet" href="/WordPaster/js/skygqbox.css" />
                <script type="text/javascript" src="/WordPaster/js/json2.min.js" charset="utf-8"></script>    
                <script type="text/javascript" src="/WordPaster/js/skygqbox.js" charset="utf-8"></script>
                <script type="text/javascript" src="/WordPaster/js/w.js" charset="utf-8"></script>
                <!--WordPaster end-->
                <script language="JavaScript" type="text/JavaScript">
                    //WordPaster开始
                    WordPaster.getInstance({ui:{render:"wordpaster-container"}});//加载控件
                    var ue = UE.getEditor('');
                    ue.ready(function ()
                    {
                        //WordPaster快捷键 Ctrl + V
                        ue.addshortcutkey({
                            "wordpaster": "ctrl+86"
                        });
                    });
                    //WordPaster结束
                </script>
                <div class="submit_model_content">
                    <asp:Button ID="BtnSave" runat="server" CssClass="submit_button" Text="<%$ Res: User_Contents_Content_BtnSave,提交 %>"
                        OnClick="BtnSave_Click" />
                    <asp:Button ID="BtnPerview" runat="server" CssClass="submit_button_six" Text="存为草稿并预览" OnClick="BtnPerview_Click" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
